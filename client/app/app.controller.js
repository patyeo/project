(function () {
    angular
        .module("ParentConnectApp")
        .controller("ParentAppCtrl", ParentAppCtrl )
        .controller("AddMemberCtrl", AddMemberCtrl)
        .controller("AddEventCtrl", AddEventCtrl)
        .controller("EditEventCtrl", EditEventCtrl);

        
    ParentAppCtrl.$inject = ['ParentAppAPI', '$uibModal', '$document', '$rootScope', '$scope', "$state", "$http"];
    AddMemberCtrl.$inject = ['ParentAppAPI', '$rootScope', '$scope'];
    AddEventCtrl.$inject = ['ParentAppAPI', 'items', '$rootScope', '$scope'];
    EditEventCtrl.$inject = ['$uibModalInstance', 'ParentAppAPI','items' ,'$rootScope', '$scope'];

    function EditEventCtrl($uibModalInstance, ParentAppAPI, items, $rootScope, $scope){
        var self = this;
        //self.items = items;
        self.deleteBook = deleteBook;
        console.log(items);
        ParentAppAPI.deleteBook(items).then((result)=>{
            console.log(result.data);
            self.employee =  result.data;
        });

        function deleteBook(){
            console.log("delete book ...");
            ParentAppAPI.deleteBook(self.books.id).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshBookList');
                $uibModalInstance.close(self.run);
            }).catch((error)=>{
                console.log(error);
            });
        }
u
    }

    function AddEventCtrl(ParentAppAPI, items, $rootScope, $scope){
        console.log("Edit Book Ctrl");
        var self = this;
        self.items = items;
        //initializeCalendar($scope);

        ParentAppAPI.getBook(items).then((result)=>{
           console.log(result.data);
           self.book =  result.data;
           
        })

        self.saveBook = saveBook;

        function saveBook(){
            console.log("save book ...");
            console.log(self.book.name);
            console.log(self.book.author);
            console.log(self.book.publish_year);
            console.log(self.book.rev_year);
            ParentAppAPI.updateBook(self.book).then((result)=>{
                console.log(result);
                $rootScope.$broadcast('refreshBookList');
             }).catch((error)=>{
                console.log(error);
             })
        }

    }


    function AddMemberCtrl(ParentAppAPI, $rootScope, $scope){
        var self = this;
        self.saveBook = saveBook;

        self.status = {
            message: ""
        };

        function saveBook(){
            console.log("save book ...");
            console.log(self.book.name);
            console.log(self.book.author);
            console.log(self.book.publish_year);
            console.log(self.book.rev_year);
            ParentAppAPI.addBook(self.book).then((result)=>{
                //console.log(result);
                console.log("Add book -> " + result.id);
                self.status.message = "Successfully added " + self.book.name + " " + self.book.rev_year;
                //$state.go('home');
                //$rootScope.$broadcast('refreshBookListFromAdd', result.data);
             }).catch((error)=>{
                console.log(error);
                //self.errorMessage = error;
                self.status.message = "Failed to add";
             })
            //$uibModalInstance.close(self.run);
        }

    }

// work on this now    
    function ParentAppCtrl(ParentAppAPI, $uibModal, $document, $rootScope, $scope, $state, $http){
        var self = this;
        self.format = "M/d/yy h:mm:ss a";
        self.events = []; // need to think what to put inside

        self.addMember = addMember;
        self.searchEvents =  searchEvents;
        self.addEvent =  addEvent;
        self.editEvent = editEvent;
        self.deleteEvent = deleteEvent;
        self.maxsize=5;
        self.totalItems=0;
        self.currentPage = 1;
        self.pageChanged = pageChanged;

    // initialize total items to 0 first. 
        self.itemsPerPage=20;
        self.currentPage=1;
        self.pageChanged = pageChanged;

// because of the duplicate codes of the EMSAppAPI, can modularize as follows:
        function searchAllEvents(searchKeyword, orderby, itemsPerPage, currentPage){
            ParentAppAPI.searchEvents(searchKeyword, orderby, itemsPerPage, currentPage).then((results)=>{
                console.log(results);
                self.event = results.data.rows;
                self.totalItems = results.data.count;
                $scope.numPages = Math.ceil(self.totalItems / self.itemsPerPage);
            }).catch((error)=>{
                console.log(error);
            });

        }
 
        function pageChanged(){
            console.log("Page changed " + self.currentPage);
            searchAllEvents(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
            console.log($scope.numPages);
        }

        $scope.$on("refreshEventList",function(){
            console.log("refresh event list "+ self.searchKeyword);
            searchAllEvents(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
        });

        $scope.$on("refreshEventListFromAdd",function(event, args){
            console.log("refresh event list from event_id "+ args.id);
            var events = [];
            events.push(args);
            self.searchKeyword = "";
            self.events = events;
        });

        function searchEvents(){
            console.log("search events  ....");
            console.log(self.orderby);
            searchAllEvents(self.searchKeyword, self.orderby, self.itemsPerPage, self.currentPage);
        }

        function addEvent(size, parentSelector){
            console.log("post add event  ....");
            var items = [];
            var parentElem = parentSelector ? 
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/addevent.html',
                controller: 'AddEventCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return items;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
            
        }
        
        function editBook(id, size, parentSelector){
            console.log("Edit Book...");
            console.log("Book ID > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/editBook.html',
                controller: 'EditBookCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

        function deleteBook(id, size, parentSelector){
            console.log("delete book...");
            console.log("Book ID > " + id);
            
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: self.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: './views/deletebook.html',
                controller: 'DeleteBookCtrl',
                controllerAs: 'ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    items: function () {
                        return id;
                    }
                }
            }).result.catch(function (resp) {
                if (['cancel', 'backdrop click', 'escape key press'].indexOf(resp) === -1)      throw resp;
            });
        }

    }





})();
