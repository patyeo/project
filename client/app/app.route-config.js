(function () {
    angular
        .module("ParentConnectApp")
        .config(ParentConnectAppConfig);
        ParentConnectAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function ParentConnectAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state("Home" ,{
                url : '/home',
                templateUrl: "/views/home.html",
                controller : 'ParentAppCtrl',
                controllerAs : 'ctrl'
            })
            .state("Signup", {
                url: "/signup",
                templateUrl: "/views/form.html",
                controller : 'AddMemberCtrl',
                controllerAs : 'ctrl'
            })
            .state("Signin", {
                url: "/signin",
                templateUrl: "/views/signin.html",
                controller : 'SigninCtrl',
                controllerAs : 'ctrl'
            })


        $urlRouterProvider.otherwise("/home");
    }

})();
